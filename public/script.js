const selectedSuggestionClass = 'selectedSuggestion'
const cookieName = 'hrenovka'

var isCrippled;

let searchField;

function debounce(func, timeout = 300) {
	let timer
	return (...args) => {
		clearTimeout(timer)
		timer = setTimeout(() => { func.apply(this, args) }, timeout)
	}
}

function wait(time) {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve();
		}, time);
	});
}

function getSuggestionURL(query) {
	return (isCrippled ? 'https://cors.bridged.cc/' : '') +
		'https://start.duckduckgo.com/ac/?q=' +
		encodeURIComponent(query) +
		'&kl=wt-wt'
}

async function getSuggestions(query) {
	if (!query) {
		return []
	}

	const url = getSuggestionURL(query)
	const result = await fetch(url)
	const suggestions = await result.json()
	return [{ phrase: query }, ...suggestions]
}

function mod(n, m) {
	return ((n % m) + m) % m;
}

function search() {
	const query = encodeURIComponent(searchField.value)
	window.location = 'https://duckduckgo.com/?q=' + query
}

async function suffocateElement(element,) {
	const left = element.offsetLeft
	const top = element.offsetTop
	element.style.animation = 'none'
	element.style.maxHeight = '30px'
	element.style.opacity = '1'
	await Promise.resolve()

	element.style.position = 'absolute'
	element.style.left = `${left}px`
	element.style.top = `${top}px`
	element.style.zIndex = '-1'
	await Promise.resolve()

	element.style.marginLeft = '32px'
	element.style.opacity = '0'
	await wait(350)

	element.parentNode?.removeChild(element)
}

async function setupSuggestions() {
	const suggestionsBox = document.getElementById('suggestionsBox')
	const fortuneNode = document.getElementById('fortune')
	var suggestions = []
	var suggestionNodes = []
	var selectedSuggestion = null
	var selectedSuggestionIndex = -1

	function clickSuggestion(e) {
		searchField.value = e.target.innerText
		search()
	}

	async function handleInput(e) {
		const query = e.target.value
		suggestions = await getSuggestions(query)

		const oldNodes = suggestionNodes
		const hasOld = !!oldNodes.length
		suggestionNodes = []
		for (const { phrase, snippet, image } of suggestions) {
			const row = document.createElement('div')
			row.classList.add('suggestion')
			if (snippet) {
				row.innerHTML =
					`<img src=${image}>` +
					`<div class="suggestionSnippet">${snippet}</div>` +
					`<div class="suggestionPhrase">${phrase}</div>`
			} else {
				row.innerText = phrase
			}

			if (hasOld) {
				row.style.animation = 'none'
			}
			row.addEventListener('click', clickSuggestion, false)
			suggestionNodes.push(row)
		}
		if (suggestionNodes.length <= 1) {
			for (const suggestion of oldNodes) {
				suffocateElement(suggestion)
			}
		} else {
			suggestionsBox.innerText = ''
		}
		for (node of suggestionNodes.slice().reverse()) {
			suggestionsBox.prepend(node)
		}

		if (suggestions.length > 1) {
			selectedSuggestionIndex = 0
			suggestionsBox.style.columnCount = suggestions.at(-1).snippet ? 2 : 1
			suggestionsBox.style.padding = '4px'
			fortuneNode.style.opacity = 0
		} else {
			selectedSuggestionIndex = -1
			suggestionsBox.style.padding = 0
			showFortune()
		}
	}

	function handleListNavigation(e) {
		if (!suggestionNodes.length) {
			return
		}

		// Down
		if (e.which === 40) {
			selectedSuggestionIndex = (selectedSuggestionIndex + 1) % suggestionNodes.length
		}

		// Up
		else if (e.which === 38) {
			selectedSuggestionIndex = mod(selectedSuggestionIndex - 1, suggestionNodes.length)
		} else {
			return
		}

		if (selectedSuggestion) {
			selectedSuggestion.classList.remove(selectedSuggestionClass)
		}
		selectedSuggestion = suggestionNodes[selectedSuggestionIndex]
		selectedSuggestion.classList.add(selectedSuggestionClass)
		selectedSuggestion.scrollIntoView({ behavior: 'smooth', block: 'center' })
		searchField.value = suggestions[selectedSuggestionIndex].phrase
		e.preventDefault()
	}

	function showFortune() {
		fortuneNode.style.opacity = 1
	}

	const inputHandler = await testIsCrippled() ?
		debounce(handleInput) :
		handleInput

	searchField.addEventListener('input', inputHandler)

	document.addEventListener('keydown', handleListNavigation, false)

	const sellSoulButton = document.getElementsByClassName('sellSoulContainer')[0]
	sellSoulButton.style.display = 'none'

	setCookie(cookieName, '\"sa senfom\"', 365)
	searchField.focus()
	showFortune()
}

async function testIsCrippled() {
	const testUrl = 'https://start.duckduckgo.com/ac/?q=test&kl=wt-wt'
	try {
		await fetch(testUrl)
	} catch {
		console.log('Browser is crippled =(')
		isCrippled = true
		return true
	}
	console.log('Browser is NOT crippled :D')
	return false
}

function setupSearch() {
	function handleEnter(e) {
		if (e.which === 13) {
			search()
		}
	}

	document.addEventListener('keydown', handleEnter, false)

	const searchButton = document.getElementById('searchButton')
	searchButton.addEventListener('click', search)
}

async function setupBackground() {
	const width = window.innerWidth
	const height = window.innerHeight
	const url = `https://picsum.photos/${width}/${height}`

	const response = await fetch(url)
	const image = await response.blob()
	const objectURL = URL.createObjectURL(image)

	const backgroundElement = document.getElementById('background')
	backgroundElement.style.backgroundImage = `url(${objectURL})`

	smallBackground.style.backgroundImage = `url(${objectURL})`
	const smallBackgroundSlammed = document.getElementById('smallBackgroundSlammed')
	smallBackgroundSlammed.style.backgroundImage = `url(${objectURL})`

	const backgroundContainer = document.getElementById('backgroundContainer')
	backgroundContainer.style.animation = 'fadeIn 5s ease 0s 1 normal both'

	const viewImageButton = document.getElementById('viewImageButton')
	viewImageButton.addEventListener('click', () => {
		window.open(objectURL, '_blank').focus()
	}, false)
	viewImageButton.style.display = 'unset'
}

function setCookie(name, value, days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "") + expires + "; path=/; Secure";
}
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function main() {
	const cookie = getCookie(cookieName)
	searchField = document.getElementById('searchField')
	setupSearch()
	if (cookie) {
		setupSuggestions()
	}
	setupBackground()
}
window.onload = main
